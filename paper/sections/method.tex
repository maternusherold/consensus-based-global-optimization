% -----------------------------------------------------------------------------
% Describe the method from head to toe.
% -----------------------------------------------------------------------------


The choice of a convex combination of the single particles as candidate is connected to a key property discussed later, having the particle distribution converge to a Dirac distribution at the optimizer. A Dirac distribution near the optimum is favorable as most of the probability is assigned to the optimizer's neighborhood. When having the particle distribution converge to a Dirac distribution thus means that, for one, the particles will be at the optimizer with high probability, and second, the system has converged, i.e. a consensus has formed. To showcase the convergence of $\rho$ to a Dirac distribution one first passes to the limit case $N \to \infty$ in \cref{eq:cbo-current-consensus}, as in \cite{pinnau2016consensusbased}, and obtains the consensus formulation as in \cref{eq:cbo-consensus-limit} where the transition holds due to the law of large numbers.

\begin{equation}
    \label{eq:cbo-consensus-limit}
    \frac{1}{\sum^N_j \omega_{\mathcal{L}}^{\alpha}(X^j)} \sum^N_j X^j \omega_{\mathcal{L}}^{\alpha}(X^j) \overset{N \to \infty}{\longrightarrow} \frac{1}{\int_{\mathbb{R}^d} \omega^{\alpha}_{\mathcal{L}} d\rho_t} \int_{\mathbb{R}^d} x \omega^{\alpha}_{\mathcal{L}} d\rho_t
\end{equation}

To proceed, one notices that $\omega^{\alpha}_{\mathcal{L}} d\rho_t$ fulfills the Laplace principle in \cref{eq:laplace-principle} by $\omega_{\mathcal{L}}^{\alpha} = \exp -\alpha \mathcal{L}$, which is the weighting function used.

\begin{equation}
    \label{eq:laplace-principle}
    \underset{\alpha \to \infty}{\lim} \left(- \frac{1}{\alpha} \log \left( \int_{\mathbb{R}^d} \exp -\alpha \mathcal{L} d \rho_t \right) \right)
\end{equation}

Thus, if $\mathcal{L}$ has a single optimizer at $X^{\ast} \in \operatorname{supp}(\rho_t)$, then the normalized measure $\eta^{\alpha} := \frac{\omega^{\alpha}_{\mathcal{L}} \rho_t}{\| \omega^{\alpha}_{\mathcal{L}}\|_{L(\rho_t)}} \in \mathcal{P}(\mathbb{R}^d)$ converges to a Dirac distribution $\delta_{X^{\ast}}$ at the optimizer $X^{\ast}$ for large $\alpha \gg 1$. Therefore, the first moment of $\eta^{\alpha}$, i.e. \cref{eq:cbo-consensus-limit}, is a good estimate for the optimizer \cite{pinnau2016consensusbased}. This motivates the choice of the weighted average as a candidate. Note, the weighted average of the particles, i.e. the consensus point, is another key difference to other metaheuristics applying swarming, where a candidate for the optimizer is chosen by taking the $\operatorname{argmin}$ over the particles, but is used in other metaheuristics such as simulated annealing \cite{Kirkpatrick671}.


When constructing an instance of the CBO, the three properties of interest are the consensus formation, i.e. having the particle distribution $\rho$ converge to a diract distribution at the optimizer, including the rate of consensus formation, the distance between the selected minimizer $X^{\prime}$ and the true position of the minimum $X^{\ast}$ and thereby implied difference to the optimal loss value \cite{fornasier2021consensusbased}. 

\begin{align*}
    &1. \quad \rho_t \to \delta_X^{\prime} \text{ as } t \to \infty \\
    &2. \quad \left\| X^{\prime} - X^{\ast} \right\|_2 \\
    &3. \quad \left\| \mathcal{L}(X^{\prime}) - \mathcal{L}(X^{\ast}) \right\|_2
\end{align*}


\subsection{Analysis of Convergence}

While analysis of the first property is possible on a microscopic level \cite{ha2019first-order-convergence}, i.e. looking at the system of single particles, it is common to do theoretical analysis of the algorithm on a macroscopic level by first passing to the mean-field limit \cite{carrillo2018analytical,carrillo2020consensusbased,fornasier2021consensusbased,pinnau2016consensusbased}. The reason for a macroscopic analysis are stronger theoretical results, describing the average behavior of the particles, and the applicability of more powerful tools while the transition between micro- and macroscopic level is still an open problem \cite{fornasier2021consensusbased}. 

A common approach for showing the convergence of $\rho$ to a Dirac distribution at $X^{\ast}$ is, following \cite{carrillo2018analytical,fornasier2021consensusbased}, to first show the convergence of $\rho_t \to \delta_{X^{\prime}} \text{ as } t \to \infty$ followed by showing $X^{\prime}$ is close to $X^{\ast}$. In \cite{carrillo2018analytical}, Carrillo et al. start by first showing the exponential reduction of the variance of the particle distribution $\rho_t$. They do so by observing the rate of change in the variance over time $\frac{d}{dt} \operatorname{Var}(\rho_t)$ and bounding the expansion.

\begin{align}
    \text{Var}(\rho_t) &= \frac{1}{2} \int \| x - \mathbb{E}(\rho_t) \|^2_2 d\rho_t \nonumber \\
    \frac{d}{dt} \text{Var}(\rho_t) \overset{\text{It\^{o}'s Lemma}}&{=} -2 \lambda \text{Var}(\rho_t) + \frac{d\sigma^2}{2} \int \|x - \bar{x}^{\ast}_t \|^2_2 d\rho_t \nonumber \\
    &= -2 \lambda \text{Var}(\rho_t) + \frac{d\sigma^2}{2} \left( 2\text{Var}(\rho_t) + \| \mathbb{E}(\rho_t) - \bar{x}^{\ast}_t \|^2_2 \right) \nonumber \\
    &= (-2 \lambda + d\sigma^2) \text{Var}(\rho_t) + \frac{d\sigma^2}{2} \| \mathbb{E}(\rho_t) - \bar{x}^{\ast}_t \|^2_2 \label{eq:variance-reduction-last}
\end{align}

Observing the first term of \cref{eq:variance-reduction-last}, $\lambda > \frac{1}{2} d \sigma^2$ needs to hold for a consensus to form. Otherwise, the variance would not contract. When analyzing the convergence, some references fix the momentaneus consensus and can stop at this point \cite{carrillo2020consensusbased}. To get a hold of the second termin in \cref{eq:variance-reduction-last}, it is important that the difference is between the mean and the weighted mean. Via the observation in \cref{eq:cbo-consensus-limit}, the weight function is $\omega^{\alpha}_{\mathcal{L}} = \exp -\alpha \mathcal{L}$ and thus, the term could be controlled by having $\alpha \to 0$ which implies $\omega^{\alpha}_{\mathcal{L}} = 1,\ \forall X^j$. Thus, the weighted average would reduce to the mean $\bar{x}^{\ast}_t \to \mathbb{E}(\rho_t)$ and the term at hand would be easy to handle \cite{fornasier2021consensusbased}. But \cref{eq:laplace-principle} showed, that $\alpha \gg 1$ gave rise to the consensus formulation, making $\alpha \to 0$ uninteresting for analysis. Another option would then be to bound the difference via Jensen's inequality (\cite{carrillo2018analytical}, Section 4.1) requiring $\lambda$ to be large and $\sigma$ to be small to maintain a decreasing variance. As those are the scales for drift and diffusion, the relevance of the random noise would thus be negligible and the system would not be stochastic anymore.

To still be able to control the difference, Carrillo et al. \cite{carrillo2018analytical} state the following assumptions on the distribution $\rho_0$ in Theorem 4.1 (rearranged according to \cite{fornasier2021consensusbased})

\begin{align}
    2 \alpha \exp (-2 \alpha \underline{\mathcal{L}})(2\lambda + \sigma^2) &< \frac{3}{4} \nonumber \\
    0 &\leq 2\lambda \|\omega^{\alpha}_{\mathcal{L}}\|^2_{L(\rho_0)} - \operatornamewithlimits{Var}(\rho_0) - 2d\sigma^2 \|\omega^{\alpha}_{\mathcal{L}}\|_{L(\rho_0)} \exp(-\alpha \underline{\mathcal{L}}) \label{eq:variance-bound}
\end{align}

where $\underline{\mathcal{L}} = \mathcal{L}(X^{\ast})$. This way, the authors are able to bound the difference by a multiple of $\operatorname{Var}(\rho_t)$ and conclude convergence. For the second step \cref{eq:cbo-consensus-limit} and \cref{eq:laplace-principle} are used to show closeness of the consensus $\bar{x}^{\ast}_t$ for $t \to \infty$ to $X^{\ast}$, i.e. $\| \mathcal{L}(\bar{x}^{\ast}_t) - \mathcal{L}(X^{\ast}) \|_2 \leq \epsilon$ for increasing $\alpha$. However, in the current case a large $\alpha$, which is also desired by the Laplace principle \cref{eq:laplace-principle}, would result in a drastic, and not realistic, restriction on $\operatorname{Var}(\rho_0)$ as, refactoring \cref{eq:variance-bound} in the simplest case with $\sigma = 0$

\begin{align*}
    0 &\leq 2\lambda \|\omega^{\alpha}_{\mathcal{L}}\|^2_{L(\rho_0)} - \operatornamewithlimits{Var}(\rho_0) - 2d\sigma^2 \|\omega^{\alpha}_{\mathcal{L}}\|_{L(\rho_0)} \exp(-\alpha \underline{\mathcal{L}}) \\
    \implies \operatorname{Var}(\rho_0) &\leq 2\lambda \|\omega^{\alpha}_{\mathcal{L}}\|^2_{L(\rho_0)} \\
    \implies \operatorname{Var}(\rho_0) &\leq 2\lambda \left( \int \exp -\alpha \mathcal{L}(\bar{x}^{\ast}_t) d\rho_0 \right)^2
\end{align*}

Solving the first condition above for $\lambda$ yields $\lambda < \frac{3}{16\alpha} \exp 2 \alpha \underline{\mathcal{L}}$ and the following bound for the initial variance

\begin{equation}
    \label{eq:init-variance-bound}
    \operatorname{Var}(\rho_0) \leq \frac{3}{8 \alpha} \left( \int \exp -\alpha (\mathcal{L}(\bar{x}^{\ast}_t) - \underline{\mathcal{L}}) d\rho_0 \right)^2.
\end{equation}

Initializing the distribution $\rho$ with decreasing variance as $\alpha$ increases according to \cref{eq:init-variance-bound}, such that $\operatorname{Var}(\rho_0) \leq \frac{3}{8\alpha}, \alpha \gg 1$ holds, is not feasible in practice as it would drastically limit the capabilities of exploring the energy landscape $\mathcal{L}$ by the particles $X^j$.


The described idea of showing the CBO's convergence is based on the strict assumptions of the initial distribution, without any on the cost function. In the following, another approach of showing the convergence states assumptions for the cost function and drops such on the initial distribution. While this is similar to other optimization techniques, e.g. when using Gradient Descent, the cost function should be continuous differentiable, it also supports a broader applicability of the algorithm. Ultimately, the concept presented by Fornasier et al. \cite{fornasier2021consensusbased} proposes a convexification of the energy landscape as the number of particles tend to infinity, similar to the limit case used in \cref{eq:cbo-consensus-limit}, for many optimization problems. Instead of showing the decrease in variance of $\rho_t$, Fornasier et al. propose to investigate the squared distance between the momentaneous consensus $\bar{x}^{\ast}$ and the optimizer $X^{\ast}$, cf. \cref{eq:squared-distance-formulation}. The idea is based on the observation that each particle performs a gradient-descent step over $\bar{x}^{\ast} \mapsto \| \bar{x}^{\ast} - X^{\ast} \|^2_2$ on average over all realizations of paths from the Wiener process.

\begin{equation}
    \label{eq:squared-distance-formulation}
    \mathcal{V}(\rho_t) := \frac{1}{2} \int \| X - X^{\ast} \|^2_2 d\rho_t
\end{equation}

For the distance to decrease for $t \to \infty$ the cost function is assumed to fulfill Lipschitz continuity as well as the coercivity in the form of \cref{eq:coercivity}. While the first bound describes the local coercivity, the second is needed to ensure that there is no possible solution close to the optimum for a far away location from the optimizer to ensure coercivity. In the case that coercivity holds already globally, \cref{eq:farfield-condition} can be dropped. 

\begin{align}
    \label{eq:coercivity}
    \| \bar{x}^{\ast} - X^{\ast} \|_2 \leq \frac{1}{\zeta} \left( \mathcal{L}(\bar{x}^{\ast}) - \mathcal{L}(X^{\ast}) \right)^{\nu},\quad &\forall X \in \mathbb{B}_{R_0}(X^{\ast}), \nu \in (0, \frac{1}{2}] \\
    \mathcal{L}_{\infty} < \mathcal{L}(\bar{x}^{\ast}) - \mathcal{L}(X^{\ast}),\quad &\forall X \in \mathbb{B}_{R_0}(X^{\ast})^c \label{eq:farfield-condition}
\end{align}

The authors substitute formulation \cref{eq:squared-distance-formulation} for $\operatorname{Var}(\rho_t)$ in the deduction of \cref{eq:variance-bound} and yield an expression independent of the distribution expectation. This enables the bound to be tightened via increasing $\alpha$, as desired, without having to sacrifice the applicability of the method.

\begin{align}
    \int \| X - \bar{x}^{\ast} \|^2_2 d\rho_t &\leq 2 \int \| X - X^{\ast} \|^2_2 d\rho_t + 2 \| \bar{x}^{\ast} - X^{\ast} \|^2_2  \nonumber \\
    &\leq 4\mathcal{V}(\rho_t) + 2 \| \bar{x}^{\ast} - X^{\ast} \|^2_2 \label{eq:variance-bound-final}
\end{align}

The tool for handeling \cref{eq:variance-bound-final} is essentially again the Laplace principle, as in \cref{eq:laplace-principle} for $\alpha \to \infty$. Also, the convergence of $\rho_t$ towards a Dirac measure is maintained as the distance between such two distributions, measures by the Wasserstein distance, is upper bounded by a multiple of \cref{eq:squared-distance-formulation}. That is, by reducing the term in \cref{eq:squared-distance-formulation}, also the distance between the particle distribution and a Dirac distribution at $X^{\ast}$ is reduced. For a detailed discussion of this formulation I refer to \cite{fornasier2021consensusbased}, Section 3.


Finally, the third property can be estimated by knowing the cost function as well as the point of convergence $X^{\prime}$, concluding the discussion about the single properties. In the following, a concrete CBO instance, aimed at high dimensional problems, is presented. 


\subsection{CBO for High Dimensions}
\label{subsec:cbo-high-dim}

After the theoretical discussion of CBO algorithms, the instances can very in many aspects, as mentioned before. In the basic formulation, each CBO instance needs many, probably costly, iterations over the complete dataset until it will start to converge. A tweak that was already applied to the popular Gradient-Descent algorithm, speeding up the training of Neural Networks, is computing statistics only on batches of data instead of the complete dataset. This yields a multiplier for the updates, i.e. optimization steps, better utilizing the data at hand and improving the computational load. Such a batched version was presented by Carrillo et al. in \cite{carrillo2020consensusbased}. They further adapted the noise term to be component-wise, allowing for a dimension-dependent noise.

The described instantiation was motivated by the dependency of $\lambda$ on the dimension $d$ as observed in \cref{eq:variance-bound-final} - assuming variance-based convergence analysis. The adapted update of the particles is presented in \cref{eq:cbo-high-dim}. While the original formulation in \cref{eq:cbo-sde} had a drift update dependent on the difference between loss at the current position and at the consensus point, the drift now is always active. Further, the noise in the diffusion term is applied component wise for each of the $i \in [d]$ dimensions, still scaled by the distance between particle and consensus. 

\begin{equation}
    \label{eq:cbo-high-dim}
    dX^j = -\lambda (X^j - \bar{x}^{\ast})dt + \sigma \sum^d_i (X^j - \bar{x}^{\ast})_i dW_i^je_i
\end{equation}

With the above formulation, the dependency of the drift parameter reduces to only the diffusion parameter as, assuming a fixed consensus $\bar{x}^{\ast} = c$

\begin{align*}
    \frac{d}{dt}\operatorname{Var}(\rho_t) &= -2 \lambda \operatorname{Var}(\rho_t) + \sigma^2 \sum^d_i \mathbb{E}(X - c)^2_i \\
    &= (-2 \lambda + \sigma^2) \operatorname{Var} (\rho_t) .
\end{align*}

Thus, $\lambda > \frac{1}{2}\sigma^2$ suffices for a contracting distribution and enables a better exploration of the energy landscape. The algorithm is described in \cref{alg:batched-cbo}, according to the reference. In the algorithm $k$ is the number of iterations, $q$ the number of minibatches per iteration, $R_{\text{step}}$ the unused particles from the prev. step, $M$ the number of particles used in a batch to compute the loss and momentaneous consensus, and $m$ the number of particles used to compute the batch loss. Note, that the batch now consists of particles and not data.

\begin{algorithm}[ht]
    \SetAlgoLined
    \algsetup{linenosize=\small}
    \small
    \begin{algorithmic}[1]

        % init variables 
        \STATE set iterations $k \in \mathbb{N}$
        \STATE generate $\{X^j\}_{j \in [N]}, X^j \sim \rho_0$
        
        \STATE  % empty line

        % iterate over dimension reduction
        \FOR{step : k} 
    
            \STATE pick $q = \left\lfloor \frac{N + R_{\text{step} -1}}{M} \right\rfloor$
            \STATE form batches $B^{\text{step}}_j, j \in [q]$
            
            \FOR {$B^{\text{step}}_j \in \{B^{\text{step}}\}$}

                \STATE compute batch loss $\hat{\mathcal{L}}^{\text{step}}_j = \frac{1}{m} \sum_{i \in A^{\text{step}}_j} l_i(X^j), \forall X^j \in B^{\text{step}}_j$
                \STATE update $\bar{x}^{\ast}_{\text{step},j}$ using $\hat{\mathcal{L}}^{\text{step}}_j$ in $omega_{\mathcal{L}}^{\alpha}$
                \STATE update $X^j$ according to \cref{eq:cbo-high-dim}

            \ENDFOR

            \STATE check stopping criteria
    
        \ENDFOR

        \STATE  % empty line
        
        % return optimal weights and min. error 
        \RETURN $\bar{x}^{\ast}, \hat{\mathcal{L}}^{\text{step}}$
    
    \end{algorithmic}

    \caption{ CBO version with batched updates and component-wise Wiener noise according to \cite{carrillo2020consensusbased}, Alg. 2.1 }
    \label{alg:batched-cbo}

\end{algorithm}



To demonstrate the algorithm's power, the authors have used the batched version of CBO to train single layer perceptron neural network with the ReLU activation function followed by a softmax classification, cf. \cref{eq:nn-mnist}, on the MNIST data set. 

\begin{equation}
    \label{eq:nn-mnist}
    f(\mathbf{x};\mathbf{W}, \mathbf{b}) = a(\operatorname{ReLU}(\mathbf{W}\mathbf{x} + \mathbf{b})),\quad \mathbf{x} \in \mathbb{R}^{784 \times 1}, \mathbf{W} \in \mathbb{R}^{10 \times 784}, \mathbf{b} \in \mathbb{R}^{10 \times 1}
\end{equation}

Having each MNIST input image being of shape $28 \times 28$ yields the input dimension of $784$\footnote{While the reference \cite{carrillo2020consensusbased} states $728$, I think $784 = 28 \cdot 28$ is correct, due to image dimensions of $28 \times 28$.}. Further, as MNIST depicts single digits, the output size is $10$. This makes it a problem with dimension $d=784 \cdot 10 + 10 = 7850$, i.e. $X^j \in \mathbb{R}^{7450}, \forall j \in [N]$, in the example, $N = 10^4$.

The authors show that their method is capable of achieving comparable results when training a neural network using CBO compared to training with stochastic gradient descent. Both models score a final accuracy around $90\%$. Taken from the original source, the results are also displayed in \cref{fig:vbo-vs-sgd-mnist-carrillo} for convenience.

For the interested reader, I also provide a Jupyter Notebook to compare the training of a Neural Network using a common stochastic gradient approach with the here discussed CBO algorithm. 

\begin{figure}[ht]
    \centering
    \includegraphics[width=.9\linewidth]{performance_cbo_vs_sgd.png}
    \caption{Final results for the comparison of a single layer neural network on the MNIST dataset. Taken from \cite{carrillo2020consensusbased}, figure 7.}
    \label{fig:vbo-vs-sgd-mnist-carrillo}
\end{figure}
