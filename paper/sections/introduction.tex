% -----------------------------------------------------------------------------
% Highlight the class of metaheuristics for optimization and where CBO fits 
% in while shading light on the possible improvement of CBO.
% -----------------------------------------------------------------------------


Finding the global optimizer of an unconstrained cost function is a challenging problem and a common task in many sciences, especially when dealing with learning algorithms. Often, such a cost function is of the general form \cref{eq:vanilla-cost} where $L$ is usually non-convex and $d$, the dimensionality, large. 

\begin{equation}
    \label{eq:vanilla-cost}
    \mathbf{x}^{\ast} = \underset{\mathbf{x} \in \mathbb{R}^d}{\operatorname{argmin}} \ \ L(\mathbf{x})
\end{equation}

Common approaches of tackling such problems can be divided into gradient-based and gradient-free methods, while gradient-based methods are especially popular in the machine learning community \cite{carrillo2018analytical,deeplearningbook-2016}. While such gradient-based methods are fairly simple to apply when having access to the cost function and its derivative, they are very dependent on the choice of its hyperparameters, i.e. variables controlling the behavior of the optimizer and only adapting to the optimization in a pre-defined manner. The problems usually arise when the cost function $L$ is indeed non-convex and a \textit{"decent"} into its valley is non-trivial. This problem becomes very obvious in the case of multi-modal objectives, such as the Ackley function, depicted in \cref{fig:ackley-3d}. Further, in many practical usecases the gradient of the cost function, or the analytical form of the algorithm, cannot be accessed, cf. \cite{intro_uq} and \cref{exp:drsm}, thus preventing the gradient to propagate. 


% Figure depicting Ackley function; used for show casing multi-modal functions
\begin{figure}[ht]
    \centering
    \includegraphics[width=.7\linewidth]{ackley_function_3d.png}
    \caption{The Ackley function, a well known benchmarking function for optimization problems, shows several local minima and is thus a perfect example for a multi-modal function. Gradient approaches will experience difficulties, when exploring the energy landscape, as a too small step size (learning rate) will get the optimizer stuck in a local minimum while a too large step size yields to overshooting the global minimum. Finally, decreasing the learning rate while optimizing introduced further hyperparameters and thus degrees of freedom in the optimization method.}
    \label{fig:ackley-3d}
\end{figure}

Further, gradient-based methods are highly dependent on the magnitude of the gradient, which is influenced by the shape of the cost function, cf. escaping saddle points, and the structure of the algorithm. Neural networks are a popular example, suffering from vanishing, and also exploding, gradients, making further optimization impossible \cite{hanin2018neural-vanishing-exploding}. \cref{fig:tanh-activation} depicts an activation function, which can easily yield vanishing gradients.

\begin{figure}[ht]
    \centering
    \includegraphics[width=.7\linewidth]{tanh_activation.png}
    \caption{Example of the vanishing gradient problem at the $tanh$ activation function in the case of neural networks. For values outside of $(-2.5,2.5)$ the gradient gets very small, yielding little feedback for optimizing the parameters.}
    \label{fig:tanh-activation}
\end{figure}



The other popular class includes metaheuristics, which are procedures aimed at selecting a heuristic that provides a sufficiently good solution to a problem. However, in general metaheuristics do not provide convergence guarantees but are capable of finding solutions with less iterations than e.g. iterative approaches \cite{metaheuristics-in-comb-optimization}. Popular representatives include the Nelder-Mead method (NM), Genetic Algorithms (GA), Simulated Annealing (SA) and Particle-Swarm Optimization (PSO) \cite{carrillo2020consensusbased,cbo-ecmi-2016}. Most of such algorithms tend to implement some kind of search and aggregation method and are said to be inspired by nature \cite{metaheuristics-survey}. Thus, it is common to call the elements traversing the cost function particles or argents which, in the case of PSO, mimic the flocking of birds to pick the particle as the optimizer which is assigned to the current best value, the estimated optimum. 

The method discussed in this work, Consensus-Based Optimization (CBO), is also part of that family where the particles/agents exploring the energy landscape aim to form a consensus where the local optimizer is located \cite{pinnau2016consensusbased}. The movement for each of the $X^j \in \mathbb{R}^d,\ j \in [N]$ particles is modelled via stochastic differential equations \cref{eq:cbo-sde}, Brownian motion to be specific, where $d$ is the dimension of $\mathcal{L}$, following some unknown distribution $X^j \sim \rho$. While the drift term assures a contraction towards the consensus, the particles experience random fluctuations via Gaussian white noise.

\begin{equation}
    \label{eq:cbo-sde}
    dX^j = -\lambda (X^j - \bar{x}^{\ast}) H^{\epsilon}(\mathcal{L}(X^j) - \mathcal{L}(\bar{x}^{\ast})) dt + \sigma |X^j - \bar{x}^{\ast}|dW^j
\end{equation}

The current consensus point $\bar{x}^{\ast}$ is modelled as weighted average over all particle positions by \cref{eq:cbo-current-consensus}. Where the choice of that specific weighting $\omega_{\mathcal{L}}^{\alpha}$ is discussed in \cref{sec:method}.

\begin{equation}
    \label{eq:cbo-current-consensus}
    \bar{x}^{\ast} = \frac{\sum^N_j X^j \omega_{\mathcal{L}}^{\alpha}(X^j)}{\sum^N_j \omega_{\mathcal{L}}^{\alpha}(X^j)}
\end{equation}

$H^{\epsilon}$ is a smoothed version the step, formally the Heaviside, function. The parameter $\epsilon$ controls the smoothness, having similar shape as the Sigmoid function as $H^{\epsilon}(\nu) = \frac{1}{1 + \exp -2 \epsilon \nu}$. Note, the drift term is thus turned off for particles with a better loss than the current consensus. Further, particles with good (worse) positions are affected less (stronger) by the drift, resulting in a contraction of the particles towards the consensus with different speeds. This can be reasoned by drawing particles to favorable regions but given them freedom to explore such favorable regions. A similar effect is seen in the diffusion term. The multiplicative noise $W^j$ is a Wiener process with the properties of independent updates and Gaussian-distributed step size. The influence of that noise is controlled by the distance of the particle to the consensus point. Thus, the further away the particle is from consensus, the larger the noise applied. This helps such particles to explore the energy landscape well. In the case of deterministic updates, such as PSO-updates, the exploration capabilities are much more dependent on the random initialization and particle number. Finally, one wants the noise do decrease when consensus happens, i.e. have the particles converging. 

CBO provides a framework for an optimization algorithm, aiming at finding a consensus as the convex combination of the single particles. Besides its stochastic nature, the form of which the updates are computed can be altered and certain adaptions can help when proving the method's convergence on multi-modal landscapes \cite{carrillo2020consensusbased,fornasier2021consensusbased} - a great benefit compared to gradient descent and the like. In the following, CBO and a batched version of it will be discussed including the choice of consensus-forming over a simple selection of the best particle and an example, showcasing the CBO's benefits regarding the applicability in practice.
