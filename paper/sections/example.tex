% -----------------------------------------------------------------------------
% Illustrate the DRSM example as well as its benefits pointing out that no 
% access to the functions derivatives, i.e. internals, are needed.
% If appropriate, add an example using a Neural Network with gradient-based 
% updates.
% -----------------------------------------------------------------------------


This section shall give the reader a good understanding of how CBO can be applied in practice. While the usage of CBO is demonstrated, the reader should notice how CBO is applied and that it does not need any more access to the target function than the parameters as a vector, representing the particles, and the associated loss. This is, as mentioned before, in contrast to common gradient-based optimizers in machine learning.

In the following, the work from \cite{lataniotis2020extending} is adapted to use Consensus-Based Optimization instead of the Particle-Swarm Optimizer. Further, in contrast to the cited work, reference solutions and comparisons are provided such that the reader can asses the methods performance. While standard literature on CBO usually investigates analytical functions for benchmarking \cite{fornasier2021consensusbased,fornasier2020consensusbased_hyper, carrillo2018analytical,pinnau2016consensusbased} as well as neural networks for their high dimensionality \cite{carrillo2020consensusbased}, this setup facilitates to show the properties, anticipated in the first paragraph.  

The example is embedded in the setting of Uncertainty Quantification (UQ) with the goal of fitting a surrogate model to an expensive black-box model, often a finite element simulation, with the goal of fast and cheap runtimes to do e.g. sensitivity analysis of the single parameters. The challenge is usually, as evaluating the simulation is expensive, a very scarce data set in high dimensions \cite{intro_uq} and nonlinear structure. Standard techniques for fitting a model like the ordinary least squares method are therefore ruled out and sparse techniques like basis adaptivity schemes, variable selection or LARS are used. Further, it has been shown that the class of polynomial chaos expansion (PCE) \cite{pce-wiener-askey} models are well suited for surrogate modelling \cite{intro_uq}. The following example shows how the selection of a sparse solver or sensitivity analysis can be skipped when fitting a PCE to the data by projecting the data into a lower dimension using a parametric function. The approach described in Example \ref{exp:drsm} is also listed in \cref{alg:drsm} for clarity.

\begin{exmp}
    \label{exp:drsm}
    Given a model $\mathcal{M}$ with input data $\mathbf{X} \in \mathbb{R}^{m \times d}$ and according model evaluations $\mathbf{y} = \mathcal{M}(\mathbf{X}),\ \mathbf{y} \in \mathbb{R}^d$. A parametric function $\phi_{\theta}: \mathbb{R}^d \to \mathbb{R}^k$, where $k \ll d$ is used to project $\mathbf{X}$ into a lower $k$-dimensional space in which a surrogate model $f_{\omega}: \mathbb{R}^k \to \mathbb{R}$ is fitted. The projection's parameters $\theta$ are optimized using CBO w.r.t. the surrogate's loss $\mathcal{L}(\mathbf{y}, \hat{f}_{\omega})$.
\end{exmp}

Note, while one could argue to pick the parameters $\theta$ for the dimension reduction $\phi_{\theta}$ w.r.t. to a metric such as distance preservation, this is not possible as the inputs, in the setting of UQ, are purely random. That is, the elements of each $\mathbf{X}^j, j \in [d]$ are sampled according to a distribution which models the randomness in the parameter $x_j$. Therefore, the data is unstructured and only the target simulation $\mathcal{M}$ introduces a structure.

\begin{algorithm}[ht]
    \SetAlgoLined
    \algsetup{linenosize=\small}
    \small
    \begin{algorithmic}[1]

        % init variables 
        \STATE $\theta_0 \gets \mathcal{U}[0,1]^d$
        \STATE $k \in \mathbb{N},\quad k \ll d$
        \STATE $\text{iterations} \gets \mathbb{N}$
        
        \STATE  % empty line

        % iterate over dimension reduction
        \FOR{step : iterations} 
    
            \STATE $d_{\text{red}} \gets \phi(\theta_{\text{step}}, k, d)$, $d_{\text{red}} \in \mathbb{R}^{m \times k}$
            \STATE $f \gets \Psi(d_{\text{red}})$
            \STATE $\hat{\epsilon} \gets \mathcal{L}(\mathbf{y}, \hat{f}_{\omega})$
            \STATE $\theta_{\text{step} + 1} \gets \mathbf{CBO}(\hat{\epsilon}, \theta_{\text{step}})$
    
        \ENDFOR

        \STATE  % empty line
        
        % return optimal weights and min. error 
        \RETURN $\theta, \hat{\epsilon}$
    
    \end{algorithmic}
    \caption{ Self-supervised projection of $\mathbf{X}$ into a lower dimension and fitting a surrogate model on that representation. }
    \label{alg:drsm}
\end{algorithm}

To showcase the working of \cref{alg:drsm}, a dataset with analytical target function is created, such that we can exactly observe the performance. The Sobol function \cref{eq:sobol} lets the user weight certain dimensions and is well suited for this case. In the dataset, only the first six dimensions have a relevant influence while the rest has negligible influence.

\begin{equation}
    \label{eq:sobol}
    y_i = \prod_j^d \frac{ |4x_i -2| + c_i }{ 1 + c_i },\ 
      \mathbf{c} = \{ 1,2,5,10,20,100, 5\cdot 10^3, \dots, 5 \cdot 10^3 \}
\end{equation}

The dataset consists of a training and validation set $\mathbf{X}_{\text{train}} \in \mathbb{R}^{300 \times 20},\ \mathbf{X}_{\text{val}} \in \mathbb{R}^{1000 \times 20}$, where the small number of training samples shall facilitate the data scarce setting.

For dimension reduction a Kernel PCA with anisotropic kernel is selected to account for variable specific parametrization as the variables have different influence, i.e. $\theta \in \mathbb{R}^{d}, d = 20$.

For comparison, a model without any prior dimension reduction and a model with only the six most influential variables was fit. The results are depicted in \cref{fig:drsm-baseline}. Following, the surrogate models were fit using \cref{alg:drsm}, iterating over the possible latent dimensions $k$, projecting into such, and fitting a model on the representation of the data in that space. The results show a drastic increase in performance compared to the model with only the six important features. The results are depicted in \cref{fig:drsm-with-reduction}. 

This example hopefully showcased the ease in applying CBO to a problem and its benefits when the methods are unknown, i.e. a black-box, or it would otherwise require a custom implementation to operate a joint, i.e. end-to-end, optimization loop. 


\begin{figure}[ht]
    \centering
    \subfloat[]{
        \includegraphics[width=0.45\linewidth]{truth_vs_pred_pce_baseline_err_0.13117.png}
    } 
    \subfloat[]{
        \includegraphics[width=0.45\linewidth]{truth_vs_pred_pce_baseline_err_0.09087.png}
    }
    \caption{Performances of reference models where (a) had no dimension reduction prior to fitting and (b) was fit on the subset of most relevant variables. }
    \label{fig:drsm-baseline}
\end{figure}


\begin{figure}[ht]
    \centering
    \subfloat[]{
        \includegraphics[width=0.45\linewidth]{truth_vs_pred_pce_drsm_anisotropic_err_0.03594.png}
    }
    \subfloat[]{
        \includegraphics[width=0.45\linewidth]{truth_vs_pred_pce_drsm_anisotropic_err_0.02946.png}
    } 
    \caption{Performances of surrogate models fit in a self-supervised fashion where the surrogate's loss determined the quality of the dimension reduction $\phi_{\theta}$. While both models perform better compared to the simple pruned model in \cref{fig:drsm-baseline}, there is a noticeable difference between the latent dimension $k=6$ and $k=8$ in performance. }
    \label{fig:drsm-with-reduction}
\end{figure}
