% Define the algorithm as a whole including the batched version. Further
% describe why certain choices werde made, exp. the average of positions 
% to pass to the mean-field limit as well as forming a Dirac measure
% at the minimizer. 


% General:
%  - uses finite number of particles X
%  - X are stochastic processes exploring domain to form a consensus at global min.
%  - X are governed by drift and diffusion
%  - drift: drag towards momentaneous consensus of the global min.
%       - i.e. contracting the convex hull of particles, aids consensus forming
%  - diffusion: random bumps to move agents around to help exploring energy landscape
%       - counteracting drift 

% Notes:
%  - per iteration t a comparison between a particles position and the - by consensus - best postion is made
%  - drift exploration towards the swarms consensus when current position worse; scaled by the distance to the consensus 
%  - diffusion random displacement to explore different regions 
%  - diffusion scaled by distance to consensus 
%  - DIFFERENCE of noe using weighted mean rather than arg min
%  - THUS: consensus is convex combination of particles 
%  - weight function omega is Gibbs distribution to the loss, i.e. prob. of being in state x
%  - FURTHER: by using this weight weight function we weight close to consensus/little loss particles higher than others, i.e. particles with bad cost have little influence 
\begin{frame}
    \frametitle{Consensus-Based Optimization}

    System of $N \in \mathbb{N}$ particles $X_t^i \in \mathbb{R}^d,\ i=1, \dots, N$, $X^i_t \sim \rho_t$ \newline

    Evolving via SDE (c.f. \cite{Pinnau_2017})
    \begin{equation}
        \label{eq:cbo-sde-particles}
        dX_t^i = - \lambda (X_t^i - v_f)H^{\epsilon}(f(X^i_t) - f(v_f)) dt + \sigma |X_t^i - v_f| dW_t^i
    \end{equation}

    where $\lambda > 0,\ \sigma \geq 0$ and 

    \begin{align}
        v_f &= \frac{1}{\sum_{i \in [N]} \omega^{\alpha}_f (X_t^i)} \sum_{i \in [N]} X_t^i \omega^{\alpha}_f (X_t^i) \label{eq:cbo-mean-position}  \\ 
        \omega_f^{\alpha}(X_t^i) &= \exp(-\alpha f(X_t^i))  \label{eq:cbo-weight}
    \end{align}

\end{frame}


\begin{frame}
    \frametitle{Properties of Interest}

    Consensus formation 
    \begin{equation}
        \label{eq:consensus-formation}
        \rho_t \to \delta_{x'} \text{ as } t \to \infty
    \end{equation}

    Distance of consensus to minimizer
    \begin{equation}
        \label{eq:conseneus-difference}
        \| x' - x^{\ast} \|_2
    \end{equation}

    Loss at consensus
    \begin{equation}
        \label{eq:consensus-loss-at-conseneus}
        \| f(x') - f(x^{\ast}) \|_2
    \end{equation}

\end{frame}


% \begin{frame}
%     \frametitle{Observations}

%     \textcolor{TUMBlue}{
%         \begin{itemize}
%             \item Particles with good postion keep their position
%             \item Strong attraction of particles far from $v_f$
%             \item Large noise exhibited by particles far from $v_f$
%         \end{itemize}
%     }

% \end{frame}



% Notes:
%  - Goal: Dirac distribution at min. -> use first moment as approx. of minimizer
%  - passing to the mean-field limit, i.e. infty particles
%  - makes analysis of algorithm easier 
%  - rho is the mean field distribution of the particle 
%  - in mean-field limit the weight func fulfills Laplace principle
%  - thus, the weighted Gibbs measure, i.e. weight func. approaches a Dirac distribution at the minimizer FOR large ALPHA
%  - weighted average, i.e. momentanous consensus is first moment of Dirac dist.
%  - if alpha too large we get arg min as in PSO but works good in practice as well
\begin{frame}
    \frametitle{Weighted Average vs. $\operatorname{argmin}$}

    \textbf{Goal:} Dirac distribution at the minimizer $x^{\ast}$

    \pause 

    \begin{equation}
        \label{eq:cbo-mean-convergence}
        \small
        \frac{1}{\sum_{i \in [N]} \omega^{\alpha}_f (X_t^i)} \sum_{i \in [N]} X_t^i \omega^{\alpha}_f (X_t^i) \quad \xrightarrow{N \to \infty} \quad \frac{1}{\int_{\mathbb{R}^d} \omega_f^{\alpha} d\rho_t} \int_{\mathbb{R}^d} x \omega_f^{\alpha} d \rho_t
    \end{equation}

    \pause 

    % so eta being a normalised measure consisting of the particle density rho and 
    % the weight function itself. we want this to converge towards a Dirac 
    % distribution.
    \small 
    $\omega_f^{\alpha} \rho_t$ fulfills \cref{eq:laplace-principle} and $\eta^{\alpha} := \frac{\omega_f^{\alpha} \rho_t}{\|\omega_f^{\alpha}\|_{L^1(\rho_t)}} \to \delta_{x^{\ast}}$ for $\alpha \gg 1$\footnote{Proof provided in Proposition 2.1 of \cite{Pinnau_2017} and \cite{carrillo2018analytical}.} \newline

    \pause

    \begin{equation}
        \label{eq:consensus-first-moment}
        \small
        v_f(\rho_t) = \int_{\mathbb{R}^d} x d\eta_t^{\alpha}
    \end{equation}

    \pause 

    \small
    \textit{but \dots} \cref{eq:cbo-mean-convergence} $\xrightarrow{\alpha \gg d \gg 1} \underset{X^i}{\operatorname{argmin}} \ f(X^i_t)$\footnote{Works well in practice but limits theoretic analysis, c.f. \cite{carrillo2020consensusbased}}\newline

\end{frame}


% Notes:
%  - usually happens on a macro- or microscopic level 
%  - convergence as consequence of decay of variance or min. sq. distances of consensus point
%  - by applying Ito's Lemma 
%  - note difference between mean and weighted mean
%  - convergence of micro- to macroscopic for N -> infty has not been fully analyzed
\begin{frame}
    \frametitle{Variance Based Convergence of CBO\footnote{according to \cite{carrillo2018analytical,fornasier2021consensusbased}}}

    \textbf{Goal:} $\rho_t \xrightarrow{t \to \infty} \delta_{x'}$ and $x' \approx x^{\ast}$

    \pause

    \begin{align*}
        \text{Var}(\rho_t) &= \frac{1}{2} \int \| x - \mathbb{E}(\rho_t) \|^2_2 d\rho_t  \\
        \frac{d}{dt} \text{Var}(\rho_t) &\overset{\text{It\^{o}'s Lemma, \cite{carrillo2018analytical}}}{=} -2 \lambda \text{Var}(\rho_t) + \frac{d\sigma^2}{2} \int \|x - v_f(\rho_t) \|^2_2 d\rho_t  \\
        &= -2 \lambda \text{Var}(\rho_t) + \frac{d\sigma^2}{2} \left( 2\text{Var}(\rho_t) + \| \mathbb{E}(\rho_t) - v_f(\rho_t) \|^2_2 \right) \\
        &= (-2 \lambda + d\sigma^2) \text{Var}(\rho_t) + \frac{d\sigma^2}{2} \| \mathbb{E}(\rho_t) - v_f(\rho_t) \|^2_2
    \end{align*}

    \pause

    $\rightarrow 2\lambda > d\sigma^2$

\end{frame}


\begin{frame}
    \frametitle{Practical Considerations}

    \begin{itemize}
        \item Computation distributable  
        \item No knowledge/access to target function
        \item Few hyperparameters
        \item Effectiveness independent of dimensions\footnote{for CBO with batched processing and component-wise geometric Brownian motion, c.f. \cite{carrillo2020consensusbased}}
        \item Computational advancements support theory, i.e. $N$ large
    \end{itemize}

\end{frame}
